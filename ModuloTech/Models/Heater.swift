//
//  Heater.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
// MARK: - Heater
class Heater: Device {
    
    var mode: Bool?
    var temperature: Int?
    
    init(id: Int, deviceName: String, mode: Bool?, temperature: Int?) {
        super.init(id: id, deviceName: deviceName)
        self.mode = mode
        self.temperature = temperature
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }

}


