//
//  Address.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
// MARK: - Address
class Address: Codable {
    
    var city: String?
    var postalCode: Int?
    let street : String?
    var streetCode: String?
    var country: String?

    init(city: String?, postalCode: Int?, street: String?, streetCode: String?, country: String?) {
        self.city = city
        self.postalCode = postalCode
        self.street = street
        self.streetCode = streetCode
        self.country = country
    }
}
