//
//  User.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

// MARK: - User
class User: Codable {
    
    var firstName: String?
    var lastName: String?
    var address: Address?
    var birthDate: Double?

    init(firstName: String?, lastName: String?, address: Address?, birthDate: Double?) {
        self.firstName = firstName
        self.lastName = lastName
        self.address = address
        self.birthDate = birthDate
    }
}
