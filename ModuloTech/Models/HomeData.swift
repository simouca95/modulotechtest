//
//  HomeData.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

class HomeData: Decodable {

    var devices: [Device]?

    let user: User?
    
    
    init(user : User?, devices : [Device]?) {
        self.user = user
        self.devices = devices
    }

}
