//
//  Device.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
// MARK: - Device
class Device: Decodable {
    let id: Int
    let deviceName: String
    //let productType : ProductType
    
    init(id: Int, deviceName: String) {
        self.id = id
        self.deviceName = deviceName
    }
}
