//
//  RollerShutter.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
// MARK: - RollerShutter
class RollerShutter: Device {
    
    var position: Int?
    
    init(id: Int, deviceName: String, position: Int?) {
        super.init(id: id, deviceName: deviceName)
        self.position = position
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}
