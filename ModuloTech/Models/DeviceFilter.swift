//
//  DeviceFilter.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation


enum DeviceFilter{
    case lightDevice
    case heaterDevice
    case rollerShutterDevice
}
