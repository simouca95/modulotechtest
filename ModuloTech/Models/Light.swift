//
//  Light.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
// MARK: - Light
class Light: Device {
    
    var mode : Bool?
    var intensity: Int?
    
    init(id: Int, deviceName: String, mode: Bool?, intensity: Int?) {
        super.init(id: id, deviceName: deviceName)
        self.mode = mode
        self.intensity = intensity
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}
