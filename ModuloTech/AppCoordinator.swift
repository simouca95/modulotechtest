//
//  AppCoordinator.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: BaseCoordinator {
    
    
    override func start() {
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            
            appDelegate.window?.rootViewController = navigationController
            appDelegate.window?.makeKeyAndVisible()
            
            let homeCoordinator = HomePageCoordinator()
            homeCoordinator.navigationController = navigationController
            start(coordinator: homeCoordinator)
        }
        
    }
}
