//
//  Config.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

final class Config {

    /// Singleton instance
    static let instance = Config()

    /// Private initializer
    private init() {}
    
    /// Base URL
    let baseURL = "https://www.storage42.com/modulotest/data.json"


}
