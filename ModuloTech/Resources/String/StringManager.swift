//
//  StringManager.swift
//  ModuloTech
//
//  Created by sami hazel on 10/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

/** Manager used to provide localized string */
protocol LocalizedStringManager {

    var error: String { get }

    var inputError: String { get }

    var on: String { get }

    var off: String { get }

    var submit: String { get }

    var firstName: String { get }

    var lastName: String { get }

    var birthDate: String { get }

    var country: String { get }

    var city: String { get }

    var street: String { get }

    var zipCode: String { get }

    var streetCode: String { get }

    var userProfile: String { get }

    var done: String { get }

    var cancel: String { get }
    
    var rollerShutter: String { get }

    var light: String { get }

    var heater: String { get }

    var erase: String { get }

}


/** Implementation of the LocalizedStringManager */
final class LocalizedStringManagerImpl: LocalizedStringManager {

    /// Singleton instance
    static let instance = LocalizedStringManagerImpl()
    
    
    
    var error: String = NSLocalizedString("error", comment: "")

    var inputError: String = NSLocalizedString("input_error", comment: "")

    var on: String = NSLocalizedString("on", comment: "")
    
    var off: String = NSLocalizedString("off", comment: "")
    
    var submit: String = NSLocalizedString("submit", comment: "")
    
    var firstName: String = NSLocalizedString("firstName", comment: "")
    
    var lastName: String = NSLocalizedString("lastName", comment: "")
    
    var birthDate: String = NSLocalizedString("birthDate", comment: "")
    
    var country: String = NSLocalizedString("country", comment: "")
    
    var city: String = NSLocalizedString("city", comment: "")
    
    var street: String = NSLocalizedString("street", comment: "")
    
    var zipCode: String = NSLocalizedString("zip_code", comment: "")
    
    var streetCode: String = NSLocalizedString("street_code", comment: "")
    
    var userProfile: String = NSLocalizedString("user_profile", comment: "")
    
    var done: String = NSLocalizedString("done", comment: "")

    var cancel: String = NSLocalizedString("cancel", comment: "")

    var light: String = NSLocalizedString("light", comment: "")
    
    var heater: String = NSLocalizedString("heater", comment: "")
    
    var rollerShutter: String = NSLocalizedString("rollerShutter", comment: "")
    
    var erase: String = NSLocalizedString("erase", comment: "")

}
