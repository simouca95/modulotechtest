//
//  UIColor+Custom.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import UIKit



/// Extension that provides the semantic colors to be used all over the mobile app.
/// based on the `Colors` assets catalog.
public extension UIColor {

    /// Enum for the color names that matches colors available in the assets catalog
    enum ColorName: String {

        case appBrown

        case appLightGrey

        case appBlack
        
        case appGreen

    }


    /** Initialize color with provided name */
    convenience init(_ name: ColorName) {
        // try to load color from name, in provided bundle
        let namedColor = UIColor(named: name.rawValue)
        if namedColor == nil {
            print("[UIColor+Custom] init(name:in), cannot find color named: \(name.rawValue)")
        }
        self.init(cgColor: (namedColor ?? .clear).cgColor) // default to clear color if named color is not available
    }

}
