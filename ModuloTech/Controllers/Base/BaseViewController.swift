//
//  BaseViewController.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    

    
    @IBAction func back(){
        
        self.navigationController?.popViewController(animated: true)
        
    }
        
}
