//
//  BaseViewModel.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

class BaseViewModel {
    
    
    // MARK: Public Properties

    let isLoading = BehaviorRelay<Bool?>(value: nil)
    
    let error = BehaviorRelay<String?>(value: nil)
    
    // MARK: Public Methods
    
    func viewDidLoad() {
    }
    
    func viewWillAppear() {
    }
    
    func viewDidAppear() {
    }
    


}
extension BaseViewModel{
    

}
