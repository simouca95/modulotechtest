//
//  UserProfileViewController.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import UIKit
import RxSwift

class UserProfileViewController: BaseViewController {
    
    @IBOutlet weak var editSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var birthDate: UITextField!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var postalCode: UITextField!
    @IBOutlet weak var streetCode: UITextField!
    
    
    
    // MARK: Private Properties
    private let disposeBag = DisposeBag()
    
    private var viewModel : UserProfileViewModel!
    
    
    private let formatter = DateFormatter()
    
    private let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstName.text = ""

        self.initView()
        self.initViewModel()
        

        updateViewControls(isEditing: false)

    }

    
    // MARK: Private Methods
    
    private func initView(){
        
        self.title = LocalizedStringManagerImpl.instance.userProfile
        
        formatter.dateFormat = "dd/MM/yyyy"
        
    }
    
    private func initViewModel() {
        
        viewModel = UserProfileViewModel()
        

        viewModel.isLoading
            .subscribe(onNext: { isLoading in
                if let isLoading = isLoading {
                    if isLoading {
                        //ProgressHelper.show()
                    } else {
                        //ProgressHelper.dismiss()
                    }
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.error
            .subscribe(onNext: { error in
                if error != nil {
                    DispatchQueue.main.async {
                        ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.error)
                    }
                    
                }
            })
            .disposed(by: disposeBag)
        
        
        viewModel.userDataSource
            .subscribe(onNext: { [weak self] user in
                if user != nil {
                    self?.firstName.text = user?.firstName
                    self?.lastName.text = user?.lastName
                    if let bd = user?.birthDate{
                        self?.birthDate.text = self?.formatter.string(from: Date(timeIntervalSince1970: Double(bd / 1000)))

                    }
                    self?.country.text = user?.address?.country
                    self?.city.text = user?.address?.city
                    self?.street.text = user?.address?.street
                    self?.streetCode.text = user?.address?.streetCode
                    self?.postalCode.text = "\(user?.address?.postalCode ?? 75000)"
                    
                }
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        
        self.updateViewControls(isEditing: sender.isOn)
        
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        
        self.isValidInfo { (isValid, firstname, lastname, bd, country, city, street, zipCode, streetCode) in
            
            if isValid{
                
                self.editSwitch.isOn = false
                
                self.updateViewControls(isEditing: false)
                
                self.viewModel.didTapSave(firstname, lastname, bd, country, city, street, zipCode, streetCode)
                
            }
        }
        
        
    }
    
    
    fileprivate func updateViewControls(isEditing : Bool) {
        // Do any additional setup after loading the view.
        
        self.saveButton.isHidden = !isEditing
        self.firstName.isUserInteractionEnabled = isEditing
        self.lastName.isUserInteractionEnabled = isEditing
        self.birthDate.isUserInteractionEnabled = isEditing
        self.street.isUserInteractionEnabled = isEditing
        self.city.isUserInteractionEnabled = isEditing
        self.country.isUserInteractionEnabled = isEditing
        self.postalCode.isUserInteractionEnabled = isEditing
        self.streetCode.isUserInteractionEnabled = isEditing
    }
    
    
    /** Validate fields */
    private func isValidInfo(completion : @escaping((_ isValid : Bool , _ firstname : String, _ lastname : String, _ birthDay : Date? , _ country : String, _ city : String, _ street : String, _ zipCode : Int , _ streetCode : String) -> Void)){
        
        
        guard let firstname = firstName.text?.replacingOccurrences(of: " ", with: ""), !firstname.isBlank else {
            ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.inputError)
            completion(false,"","",nil,"","","",0,"")
            return
        }
        
        guard let lastname = lastName.text?.replacingOccurrences(of: " ", with: ""), !lastname.isBlank else {
            ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.inputError)
            completion(false,"","",nil,"","","",0,"")
            return
        }
        
        var bdDate : Date!
        
        if let birthDay = self.birthDate.text{
            
            
            bdDate = formatter.date(from: birthDay)
            
        }
        guard let country = country.text?.replacingOccurrences(of: " ", with: ""), !country.isBlank else {
            ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.inputError)
            completion(false,"","",nil,"","","",0,"")
            return
        }
        guard let city = city.text?.replacingOccurrences(of: " ", with: ""), !city.isBlank else {
            ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.inputError)
            completion(false,"","",nil,"","","",0,"")
            return
        }
        guard let street = street.text?.replacingOccurrences(of: " ", with: ""), !street.isBlank else {
            ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.inputError)
            completion(false,"","",nil,"","","",0,"")
            return
        }
        guard let zipCodeStr = postalCode.text?.replacingOccurrences(of: " ", with: ""), let zipCode = Int(zipCodeStr)  else {
            ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.inputError)
            completion(false,"","",nil,"","","",0,"")
            return
        }
        guard let streetCode = streetCode.text?.replacingOccurrences(of: " ", with: ""), !streetCode.isBlank else {
            ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.inputError)
            completion(false,"","",nil,"","","",0,"")
            return
        }
        
        
        completion(true,firstname,lastname,bdDate,country,city,street,zipCode,streetCode)
        
        
    }
    
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: LocalizedStringManagerImpl.instance.done, style: UIBarButtonItem.Style.plain, target: self, action: #selector(donedatePicker))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: LocalizedStringManagerImpl.instance.cancel, style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        birthDate.inputAccessoryView = toolbar
        // add datepicker to textField
        birthDate.inputView = datePicker
        
    }
    @objc func donedatePicker(){
        
        //For date formate
        birthDate.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
}
