//
//  UserProfileViewModel.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class UserProfileViewModel: BaseViewModel {

    let success = PublishRelay<Bool>()
    
    var userDataSource : Observable<User?>! = DataManager.shared.userData.asObservable()
    
    private let disposeBag = DisposeBag()
    
    
    func didTapSave( _ firstname : String, _ lastname : String, _ birthDay : Date? , _ country : String, _ city : String, _ street : String, _ zipCode : Int , _ streetCode : String){
        
        let updatedAddress = Address(city: city, postalCode: zipCode, street: street, streetCode: streetCode, country: country)
        let updatedUser = User(firstName: firstname, lastName: lastname, address: updatedAddress, birthDate: birthDay?.timeIntervalSince1970 ?? Date().timeIntervalSinceNow)
        
        
        DataManager.shared.updateUser(user: updatedUser)
        
    }
}

extension UserProfileViewModel {
    
    private func onSuccess(_ result : HomeData?) {
        
        self.isLoading.accept(false)

        
    }
    
    private func onFail(_ error: Error?) {
        self.isLoading.accept(false)
        self.error.accept(error?.localizedDescription)
    }
}
