//
//  UserProfileViewCoordinator.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

class UserProfileViewCoordinator: BaseCoordinator {

    override func start() {
        
        let vc = UserProfileViewController()
        
        self.navigationController.pushViewController(vc, animated: true)
    }
}
