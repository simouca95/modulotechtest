//
//  HomeViewController.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: BaseViewController {
    
    
    // MARK: Outlets
    @IBOutlet weak var devicesTableView : UITableView!
    
    
    // MARK: Private Properties
    private let disposeBag = DisposeBag()
    
    var viewModel : HomeViewModel!
    
    private var devices : [Device] {
        get{
            return self.viewModel.devicesDataSource.value 
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initView()
        self.initViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func initView() {
        
        let filterBtn = UIButton(type: .custom)
        filterBtn.setImage(UIImage(named:"filter"), for: .normal)
        filterBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        filterBtn.addTarget(self, action: #selector(openFilter), for: .touchUpInside)
        let itemFilter = UIBarButtonItem(customView: filterBtn)

        let userBtn = UIButton(type: .custom)
        userBtn.setImage(UIImage(named: "user"), for: .normal)
        userBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        userBtn.addTarget(self, action: #selector(openUserProfile), for: .touchUpInside)
        let itemUser = UIBarButtonItem(customView: userBtn)

        self.navigationItem.setRightBarButton(itemFilter, animated: false)
        
        self.navigationItem.setLeftBarButton(itemUser, animated: false)

    }
    
    @objc func openFilter(){
        
        self.viewModel.didSelectFilters.accept(Void())
        
    }
    
    @objc func openUserProfile(){

        self.viewModel.didSelectUserProfile.accept(Void())
    }
    
    func initTableView()  {
        
        self.devicesTableView.register(UINib(nibName: "DeviceTableViewCell", bundle: nil),
                                       forCellReuseIdentifier:DeviceTableViewCell.identifier )
        
        self.devicesTableView.delegate = self
                
        self.viewModel.devicesDataSource.bind(to: self.devicesTableView.rx.items(cellIdentifier: DeviceTableViewCell.identifier, cellType: DeviceTableViewCell.self)) { (item, element, cell) in
                cell.initCell(device: element)
        }
        .disposed(by: disposeBag)
        
        devicesTableView.rx.modelSelected(Device.self).subscribe(onNext: { item in
            self.viewModel.didSelectDevice.accept(item)

        }).disposed(by: disposeBag)

        
        devicesTableView.rx.itemDeleted.subscribe{ [unowned self] event in
            
            if let indexPath = event.element {
                
                // delete the table view row
                //self.devicesTableView.deleteRows(at: [indexPath], with: .fade)
                
                 // remove the item from the data model
                self.viewModel.deleteDevice(deviceToDelete: self.devices[indexPath.row])
                

            }
  
        }.disposed(by: disposeBag)


    }
    
    // MARK: Private Methods
    
    private func initViewModel() {
                
        viewModel.viewDidLoad()
        
        self.initTableView()
        
        viewModel.isLoading.asObservable()
            .subscribe(onNext: { isLoading in
                if let isLoading = isLoading {
                    if isLoading {
                        //ProgressHelper.show()
                    } else {
                        //ProgressHelper.dismiss()
                    }
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.error.asObservable()
            .subscribe(onNext: { error in
                if error != nil {
                    DispatchQueue.main.async {
                        ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.error)
                    }
                    
                }
            })
            .disposed(by: disposeBag)
    }
    
}

extension HomeViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }

    
    // this method handles row deletion
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

         if editingStyle == .delete {

             // remove the item from the data model
            self.viewModel.deleteDevice(deviceToDelete: self.devices[indexPath.row])

             // delete the table view row
             tableView.deleteRows(at: [indexPath], with: .fade)

         }
     }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return LocalizedStringManagerImpl.instance.erase
    }
    
}
