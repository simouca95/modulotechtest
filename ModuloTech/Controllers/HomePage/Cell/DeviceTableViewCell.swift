//
//  DeviceTableViewCell.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell {
    
    static let identifier = "deviceCell"
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var deviceTypeImage: UIImageView!
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var state: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initCell(device : Device){
        
        self.deviceName.text = device.deviceName
        
        switch device {
            
        case let heater as Heater:
            
            self.deviceTypeImage.image = UIImage(named: "thermometer")
            self.value.text = "\(heater.temperature ?? 7)"
            self.state.text = (heater.mode ?? false) ? "ON" : "OFF"
            self.state.isHidden = false

        case let rollerShutter as RollerShutter:
            
            self.deviceTypeImage.image = UIImage(named: "roller-shutter-door")
            self.value.text = "\(rollerShutter.position ?? 0)"
            self.state.isHidden = true
            
        case let light as Light:
            
            self.deviceTypeImage.image = UIImage(named: "idea")
            self.value.text = "\(light.intensity ?? 0)"
            self.state.text = (light.mode ?? false) ? "ON" : "OFF"
            self.state.isHidden = false

        default:
            break
        }
        
    }
    
}
