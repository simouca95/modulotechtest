//
//  HomePageCoordinator.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class HomePageCoordinator: BaseCoordinator {
    
    private let disposeBag = DisposeBag()

    override func start() {
        
        let initialViewController = HomeViewController()
    
        let homeViewModel = HomeViewModel()
        
        initialViewController.viewModel = homeViewModel
        
        initialViewController.viewModel.didSelectDevice.subscribe(onNext: { device in
            self.selectDeviceDetail(device)
        })
        .disposed(by: disposeBag)
        
        initialViewController.viewModel.didSelectUserProfile.subscribe(onNext: { _ in
            self.selectProfile()
        })
        .disposed(by: disposeBag)
        
        initialViewController.viewModel.didSelectFilters.subscribe(onNext: { _ in
            self.selectFilters(filters: homeViewModel.deviceFilters)
            })
            .disposed(by: disposeBag)
        
        self.navigationController.viewControllers = [initialViewController]
    }
    
    func selectDeviceDetail(_ device : Device?){
        
        switch device {
            
        case is Heater:
            let coordinator = HeaterCoordinator(device: device as! Heater)
            coordinator.navigationController  = navigationController
            start(coordinator: coordinator)

        case is Light:
            let coordinator = LightViewCoordinator(device: device as! Light)
            coordinator.navigationController  = navigationController
            start(coordinator: coordinator)

        case is RollerShutter:
            let coordinator = RollerShutterCoordinator(device: device as! RollerShutter)
            coordinator.navigationController  = navigationController
            start(coordinator: coordinator)

        default:
            break
        }
    }
    
    func selectProfile(){
        let coordinator = UserProfileViewCoordinator()
        coordinator.navigationController  = navigationController
        start(coordinator: coordinator)
    }
    
    func selectFilters(filters : BehaviorRelay<[DeviceFilter : Bool]>){
        let coordinator = DeviceFilterViewCoordinator(filters: filters)
        coordinator.navigationController = navigationController
        start(coordinator: coordinator)
    }
}
