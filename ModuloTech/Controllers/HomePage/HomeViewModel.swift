//
//  HomeViewModel.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

class HomeViewModel : BaseViewModel{
    
    let devicesDataSource = BehaviorRelay<[Device]>(value: [])
    
    
    var deviceFilters = BehaviorRelay<[DeviceFilter : Bool]>(value: [.lightDevice : true,
                                                                     .heaterDevice : true,
                                                                     .rollerShutterDevice : true])
    
    let didSelectUserProfile = PublishRelay<Void>()
    
    let didSelectFilters = PublishRelay<Void>()
    
    let didSelectDevice = BehaviorRelay<Device?>(value: nil)

    private let disposeBag = DisposeBag()
    

    func applyFilters(){
        if var devices = DataManager.shared.devicesData.value {
            
            devices = devices.filter { (device) -> Bool in
                switch device{
                case is Heater :
                    return self.deviceFilters.value[.heaterDevice] ?? true
                    
                case is Light :
                    return self.deviceFilters.value[.lightDevice] ?? true
                    
                case is RollerShutter :
                    return self.deviceFilters.value[.rollerShutterDevice] ?? true
                    
                default:
                    return true
                }
            }
            
            self.devicesDataSource.accept(devices)
            
        }else{
            self.devicesDataSource.accept([])
        }
    }
    
    override func viewDidLoad() {
        
        self.isLoading.accept(true)
        
        DataManager.shared.loadHomeData {
            
            self.isLoading.accept(false)
            
        }
        
        DataManager.shared.devicesData.subscribe(onNext: { [weak self] _ in
            self?.applyFilters()
        }).disposed(by: disposeBag)
        
        deviceFilters.subscribe(onNext: { [weak self] _ in
            self?.applyFilters()
        }).disposed(by: disposeBag)
        
        
    }
    
    func deleteDevice(deviceToDelete : Device) {
        DataManager.shared.deleteDevice(device: deviceToDelete)
        
    }
    
}

extension HomeViewModel {
        
    private func onFail(_ error: Error?) {
        self.isLoading.accept(false)
        self.error.accept(error?.localizedDescription)
    }
}
