//
//  LightViewModel.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class LightViewModel{
    
    var lightRelay = BehaviorRelay<Light?>(value: nil)

    init(light : Light){
        
        lightRelay.accept(light)
        
    }
    
    func sliderDidChangeValues(value : Int){
        
        let light = lightRelay.value
        
        light?.intensity = value
        
        lightRelay.accept(light)
        
        DataManager.shared.refreshDevices()
        
    }
    
    func lightDidChangeMode(){
        
        let light = lightRelay.value
        
        light?.mode = !(light?.mode ?? true)
        
        lightRelay.accept(light)
        
        DataManager.shared.refreshDevices()

    }
    
}

