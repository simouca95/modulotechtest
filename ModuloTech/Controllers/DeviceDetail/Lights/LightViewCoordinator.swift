//
//  LightViewCoordinator.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

class LightViewCoordinator: BaseCoordinator {
    
    private let lightDevice : Light
    
    init(device : Light) {
        self.lightDevice = device
        
    }
    override func start() {
        
        let vc = LightViewController()
        vc.loadViewIfNeeded()
        vc.initView(device: lightDevice)
        self.navigationController.pushViewController(vc, animated: true)
        
    }
}
