//
//  HeaterViewModel.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class HeaterViewModel{
    
    var heaterRelay = BehaviorRelay<Heater?>(value: nil)

    init(heater : Heater){
        
        heaterRelay.accept(heater)
        
    }
    
    func sliderDidChangeValues(value : Int){
        
        let heater = heaterRelay.value
        
        heater?.temperature = value
        
        heaterRelay.accept(heater)
        
        DataManager.shared.refreshDevices()
        
    }
    
    func heaterDidChangeMode(){
        
        let heater = heaterRelay.value
        
        heater?.mode = !(heater?.mode ?? true)

        heaterRelay.accept(heater)
        
        DataManager.shared.refreshDevices()

    }
    
}

