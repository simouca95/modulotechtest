//
//  HeaterCoordinator.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
class HeaterCoordinator: BaseCoordinator {
    
    private let heaterDevice : Heater
    
    init(device : Heater) {
        self.heaterDevice = device
        
    }
    override func start() {
        
        let vc = HeaterViewController()
        vc.loadViewIfNeeded()
        vc.initView(device: heaterDevice)
        self.navigationController.pushViewController(vc, animated: true)
        
    }
}
