//
//  HeaterViewController.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import UIKit
import RxSwift

class HeaterViewController: BaseViewController {

    
    @IBOutlet weak var sliderContainer: UIView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var onOffButton: UIButton!

    
    
    
    // MARK: Private Properties
    private let disposeBag = DisposeBag()
    
    private var viewModel : HeaterViewModel!
    
    private var slider : VerticalSlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initSlider()
    }

    
    func initView(device : Heater){
        
        self.viewModel = HeaterViewModel(heater: device)

        self.viewModel.heaterRelay.subscribe(onNext: { [weak self] (heaterDevice) in
            
            self?.deviceName.text = heaterDevice?.deviceName
            
            if heaterDevice?.mode ?? true{
                
                self?.onOffButton.setTitle(LocalizedStringManagerImpl.instance.on, for:.normal)
                self?.onOffButton.setTitleColor(.white, for: .normal)
                self?.onOffButton.backgroundColor =  UIColor(.appGreen)
                self?.slider.isEnabled = true


            }else {
                self?.onOffButton.setTitle(LocalizedStringManagerImpl.instance.off, for:.normal)
                self?.onOffButton.setTitleColor(.black, for: .normal)
                self?.onOffButton.backgroundColor = UIColor.red

                self?.slider.isEnabled = false

            }
            
            self?.temperature.text = "\(heaterDevice?.temperature ?? 0)"
            
            self?.slider.value = roundf(Float(heaterDevice?.temperature ?? 0) / 10)
        })
        .disposed(by: disposeBag)
    }
    
    func initSlider()  {
           
           self.slider = VerticalSlider()
           slider.transform = CGAffineTransform(rotationAngle: -.pi/2)
           
           self.sliderContainer.addSubview(self.slider)

           slider.frame = sliderContainer.bounds
           
           slider.minimumValue = 0
           slider.maximumValue = 10
           slider.addTarget(self, action: #selector(self.sliderValueChanged(_:)), for: .valueChanged)
           
       }
       
       @objc func sliderValueChanged(_ slider : VerticalSlider){
                   
           self.viewModel.sliderDidChangeValues(value: Int(roundf(slider.value)) * 10)
           
           
       }
       
       @IBAction func didTapOnOffButton(_ sender: UIButton) {
                   
           self.viewModel.heaterDidChangeMode()
       }

}
