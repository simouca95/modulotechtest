//
//  RollerShutterCoordinator.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
class RollerShutterCoordinator: BaseCoordinator {
    
    private let rollerShutterDevice : RollerShutter
    
    init(device : RollerShutter) {
        self.rollerShutterDevice = device
        
    }
    override func start() {
        
        let vc = RollerShutterViewController()
        vc.loadViewIfNeeded()
        vc.initView(device: rollerShutterDevice)
        self.navigationController.pushViewController(vc, animated: true)
        
    }
}
