//
//  RollerShutterViewController.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import UIKit
import RxSwift

class RollerShutterViewController: BaseViewController {

    
    @IBOutlet weak var sliderContainer: UIView!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var deviceName: UILabel!
    
    
    // MARK: Private Properties
    private let disposeBag = DisposeBag()
    
    private var viewModel : RollerShutterViewModel!
    
    private var slider : VerticalSlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initSlider()
    }

    
    func initView(device : RollerShutter){
        
        self.viewModel = RollerShutterViewModel(rollerShutter: device)

        self.viewModel.rollerShutterRelay.subscribe(onNext: { [weak self] (rollerDevice) in
            
            self?.deviceName.text = rollerDevice?.deviceName
            
                        
            self?.position.text = "\(rollerDevice?.position ?? 0)"
            
            self?.slider.value = roundf(Float(rollerDevice?.position ?? 0) / 10)
        })
        .disposed(by: disposeBag)
    }
    
    func initSlider()  {
           
           self.slider = VerticalSlider()
           slider.transform = CGAffineTransform(rotationAngle: -.pi/2)
           
           self.sliderContainer.addSubview(self.slider)

           slider.frame = sliderContainer.bounds
           
           slider.minimumValue = 0
           slider.maximumValue = 10
           slider.addTarget(self, action: #selector(self.sliderValueChanged(_:)), for: .valueChanged)
           
       }
       
       @objc func sliderValueChanged(_ slider : VerticalSlider){
                   
           self.viewModel.rollerShutterDidChangePosition(value: Int(roundf(slider.value)) * 10)
           
           
       }
       

}
