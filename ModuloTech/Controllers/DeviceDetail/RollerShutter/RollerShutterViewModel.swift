//
//  RollerShutterViewModel.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class RollerShutterViewModel{
    
    var rollerShutterRelay = BehaviorRelay<RollerShutter?>(value: nil)

    init(rollerShutter : RollerShutter){
        
        rollerShutterRelay.accept(rollerShutter)
        
    }
    
    func rollerShutterDidChangePosition(value : Int){
        
        let rollerShutter = rollerShutterRelay.value
        
        rollerShutter?.position = value
        
        rollerShutterRelay.accept(rollerShutter)
        
        DataManager.shared.refreshDevices()
        
    }

    
}

