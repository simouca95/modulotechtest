//
//  DeviceFilterViewController.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import UIKit
import RxSwift

class DeviceFilterViewController: BaseViewController {

    @IBOutlet weak var filtersTableView: UITableView!
    
    
    // MARK: Private Properties
    private let disposeBag = DisposeBag()
    
    var viewModel : DeviceFilterViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initViewModel()
    }

  
    @IBAction func didTapDone(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func initTableView()  {
        
        self.filtersTableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil),
                                       forCellReuseIdentifier:FilterTableViewCell.identifier )
        
        self.filtersTableView.delegate = self
        
        //self.devicesTableView.dataSource = self
        
        self.viewModel.filters
            .bind(to: self.filtersTableView.rx.items(cellIdentifier: FilterTableViewCell.identifier, cellType: FilterTableViewCell.self)){(item, element, cell) in
                
                cell.initCell(filter: element.key, isOn: element.value)
                
                cell.didTapSwitcher = {
                    
                    self.viewModel.didTapFilter(filter: element.key)
                }
        }
        .disposed(by: disposeBag)
    }
    
    // MARK: Private Methods
    
    private func initViewModel() {
                        
        self.initTableView()
        
        viewModel.isLoading.asObservable()
            .subscribe(onNext: { isLoading in
                if let isLoading = isLoading {
                    if isLoading {
                        //ProgressHelper.show()
                    } else {
                        //ProgressHelper.dismiss()
                    }
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.error.asObservable()
            .subscribe(onNext: { error in
                if error != nil {
                    DispatchQueue.main.async {
                        ToastView.shared.short(self.view, txt_msg: LocalizedStringManagerImpl.instance.error)
                    }
                    
                }
            })
            .disposed(by: disposeBag)
    }
    
}

extension DeviceFilterViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
                
    }
    
}

