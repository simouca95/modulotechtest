//
//  DeviceFilterViewCoordinator.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class DeviceFilterViewCoordinator: BaseCoordinator {

    var filters : BehaviorRelay<[DeviceFilter : Bool]>
    
    init(filters : BehaviorRelay<[DeviceFilter : Bool]>) {
        self.filters = filters
    }
    
    override func start() {
        let deviceFilterVC = DeviceFilterViewController()
        
        deviceFilterVC.modalPresentationStyle = .overCurrentContext
        
        deviceFilterVC.modalTransitionStyle = .crossDissolve

        let deviceFilterViewModel = DeviceFilterViewModel(filters: filters)
                
        deviceFilterVC.viewModel = deviceFilterViewModel
        
        self.navigationController.topViewController?.present(deviceFilterVC, animated: true, completion: nil)
    }
}
