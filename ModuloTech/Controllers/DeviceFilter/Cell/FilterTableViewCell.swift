//
//  FilterTableViewCell.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    static let identifier = "filterCell"

    @IBOutlet weak var filterName: UILabel!
    
    @IBOutlet weak var filterSwitcher: UISwitch!
        
    var didTapSwitcher : () -> Void = {() in}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func initCell(filter : DeviceFilter , isOn : Bool){
        
        switch filter {
        case .heaterDevice:
            self.filterName.text = LocalizedStringManagerImpl.instance.heater
            
        case .lightDevice:
            self.filterName.text = LocalizedStringManagerImpl.instance.light
            
        case .rollerShutterDevice:
            self.filterName.text = LocalizedStringManagerImpl.instance.rollerShutter

        }
        
        self.filterSwitcher.isOn = isOn
        
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {

        self.didTapSwitcher()
        
        
    }
    
}
