//
//  DeviceFilterViewModel.swift
//  ModuloTech
//
//  Created by sami hazel on 12/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class DeviceFilterViewModel: BaseViewModel {
    
    private let disposeBag = DisposeBag()
    
    
    var filters : BehaviorRelay<[DeviceFilter : Bool]>
    
    init(filters : BehaviorRelay<[DeviceFilter : Bool]>) {
        self.filters = filters
    }
    
    
    func didTapFilter(filter : DeviceFilter){
        
        var filtersDevices = filters.value
        
        filtersDevices[filter] = !(filtersDevices[filter] ?? true)
        
        filters.accept(filtersDevices)
        
    }
}

extension DeviceFilterViewModel {
    
    private func onFail(_ error: Error?) {
        self.isLoading.accept(false)
        self.error.accept(error?.localizedDescription)
    }
}
