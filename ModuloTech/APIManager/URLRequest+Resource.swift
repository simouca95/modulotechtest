//
//  URLRequest+Resource.swift
//  ModuloTech
//
//  Created by sami hazel on 13/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

extension URLRequest {
    
    init(_ resource: Resource) {
        self.init(url: resource.url)
        self.httpMethod = resource.method
    }
    
}
