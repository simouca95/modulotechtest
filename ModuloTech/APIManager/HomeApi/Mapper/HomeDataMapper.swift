//
//  HomeDataMapper.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

class HomeDataMapper{
    func map(dto : HomeDataDto) -> HomeData{
        
        return HomeData(user: mapUser(dto: dto.user), devices: mapDevices(dto: dto.devices))
        
    }
    
    private func mapUser(dto : UserDto) -> User?{
        
        //mapping the user model

        return User(firstName: dto.firstName,
                    lastName: dto.lastName,
                    address: mapAddress(dto: dto.address),
                    birthDate: dto.birthDate)
    }
    
    private func mapAddress(dto : AddressDto?) -> Address?{
        
        //        guard let country = dto.country,
        //            let city = dto.city,
        //            let postalCode = dto.postalCode,
        //            let street = dto.street,
        //            let streetCode = dto.streetCode
        //        else { return nil}
        
        //mapping the address model

        return Address(city: dto?.city,
                       postalCode: dto?.postalCode,
                       street: dto?.street,
                       streetCode: dto?.streetCode,
                       country: dto?.country)
        
    }
    
    private func mapDevices(dto : [DeviceDto]?) -> [Device]?{
        
        var devices = [Device]()
        
        dto?.forEach({ (device) in
            
            switch device.productType{
            case .heater :
                devices.append(Heater(id: device.id, deviceName: device.deviceName, mode: device.mode == "ON", temperature: device.temperature))
            case .light :
                devices.append(Light(id: device.id, deviceName: device.deviceName, mode: device.mode == "ON", intensity: device.intensity))

            case .rollerShutter :
                devices.append(RollerShutter(id: device.id, deviceName: device.deviceName, position: device.position))


            }
            
        })
        
        return devices
        
    }
    
}



