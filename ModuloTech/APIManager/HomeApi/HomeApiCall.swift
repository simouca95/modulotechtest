//
//  HomeApiCall.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

struct HomeApiCall {
    
    /// Singleton instance
    static let instance = HomeApiCall()
    
    /// Private initializer
    private init() {}
    
    private let apiManager = ApiCallManager.instance
    
    private let url = URL(string: Config.instance.baseURL)!


    func loadData(onSuccess successCallback:((HomeData?) -> Void)?,
                  onFailure failureCallback:((_ error: Error?) -> Void)?) {
        
        let resource = Resource(url: url)

        apiManager.createRequest(resource) { (result) in
            switch result {
            case .success(let data):
                
                do {
                    
                    let homeResponse = try JSONDecoder().decode(HomeDataDto.self, from: data)
                    
                    
                    successCallback?(HomeDataMapper().map(dto: homeResponse))
                    
//                    do {
//                        // make sure this JSON is in the format we expect
//                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
//                            // try to read out a string array
//                            if let user = json["user"] as? [User.CodingKeys: Any] {
//                                let user = User(from: user)
//                            }
//                            if let devices = json["devices"] as? [[String : Any]] {
//
//                                devices.forEach { (device) in
//                                    switch device[Device.CodingKeys.productType.rawValue] as! String{
//
//                                    case ProductType.heater.rawValue : break;
//
//                                    case ProductType.light.rawValue : break;
//
//                                    case ProductType.rollerShutter.rawValue : break;
//
//
//                                    default:
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//                    } catch let error as NSError {
//                        print("Failed to load: \(error.localizedDescription)")
//                    }


                } catch let error {
                    failureCallback?(error)

                }
                
                
            case .failure(let error):
                print("\(self) retrive error: \(error) from resource: \(resource)")
                
                failureCallback?(error)
            }
        }
    }

}
