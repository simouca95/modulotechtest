//
//  UserDto.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

// MARK: - UserDto
struct UserDto: Decodable {
    
    let firstName: String?
    let lastName: String?
    let address: AddressDto?
    let birthDate: Double?
}
