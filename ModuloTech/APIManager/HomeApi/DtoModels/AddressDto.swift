//
//  AddressDto.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

// MARK: - AddressDto
struct AddressDto: Decodable {
    
    let city: String?
    let postalCode: Int?
    let street : String?
    let streetCode: String?
    let country: String?
}
