//
//  DeviceDto.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

struct DeviceDto: Decodable {
    let id: Int
    let deviceName: String
    let productType : ProductTypeDto
    let mode: String?
    let temperature: Int?
    let intensity: Int?
    let position: Int?
}
enum ProductTypeDto: String, Decodable {
    case heater = "Heater"
    case light = "Light"
    case rollerShutter = "RollerShutter"
}
