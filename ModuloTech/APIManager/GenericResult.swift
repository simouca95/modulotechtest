//
//  GenericResult.swift
//  ModuloTech
//
//  Created by sami hazel on 13/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation


enum Result<T> {
    case success(T)
    case failure(Error)
}

enum APICallError: Error {
    case noData
}


