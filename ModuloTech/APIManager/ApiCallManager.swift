//
//  ApiCallManager.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

struct ApiCallManager {
    
    
    /// Singleton instance
    static let instance = ApiCallManager()

    /// Private initializer
    private init() {}
    
    let session = URLSession.shared

    // MARK: - Request Handler
    
    /** Centralize all the `URLSession` calls */
    
    func createRequest(_ resource: Resource, result: @escaping ((Result<Data>) -> Void)){
        
        let request = URLRequest(resource)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                result(.failure(APICallError.noData))
                return
            }
            if let error = error {
                result(.failure(error))
                return
            }
            result(.success(data))
            
        }
        task.resume()
        
    }
    
}
