//
//  HomeApiResponseDto.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

class HomeDataDto: Decodable {

    var devices: [DeviceDto]

    let user: UserDto

}


