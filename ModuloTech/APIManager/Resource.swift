//
//  Resource.swift
//  ModuloTech
//
//  Created by sami hazel on 13/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation

struct Resource {
    let url: URL
    let method: String = "GET"
}
