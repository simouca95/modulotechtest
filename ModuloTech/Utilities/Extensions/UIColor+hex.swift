//
//  UIColor+hex.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import UIKit



extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    func colorByDarkeningColorWithValue(value:CGFloat)->UIColor{
        
        let totalComponents = self.cgColor.numberOfComponents
        let isGreyscale = (totalComponents == 2) ? true : false
        
        let oldComponents = self.cgColor.components
        var newComponents = [CGFloat]()
        
        if (isGreyscale) {
            newComponents.append((oldComponents?[0])! - value < 0.0 ? 0.0 : (oldComponents?[0])! - value)
            newComponents.append((oldComponents?[0])! - value < 0.0 ? 0.0 : (oldComponents?[0])! - value)
            newComponents.append((oldComponents?[0])! - value < 0.0 ? 0.0 : (oldComponents?[0])! - value)
            newComponents.append((oldComponents?[1])!)
        }else {
            newComponents.append((oldComponents?[0])! - value < 0.0 ? 0.0 : (oldComponents?[0])! - value)
            newComponents.append((oldComponents?[1])! - value < 0.0 ? 0.0 : (oldComponents?[1])! - value)
            newComponents.append((oldComponents?[2])! - value < 0.0 ? 0.0 : (oldComponents?[2])! - value)
            newComponents.append((oldComponents?[3])!)
        }
        
        let colorSpace = CGColorSpaceCreateDeviceRGB();
        let newColor = CGColor(colorSpace: colorSpace, components: newComponents);
        let retColor = UIColor(cgColor: newColor!)
        
        return retColor;
    }
    convenience init(hex: Int) {
        self.init(hex: hex, a: 1.0)
    }
    
    convenience init(hex: Int, a: CGFloat) {
        self.init(r: (hex >> 16) & 0xff, g: (hex >> 8) & 0xff, b: hex & 0xff, a: a)
    }
    
    convenience init(r: Int, g: Int, b: Int) {
        self.init(r: r, g: g, b: b, a: 1.0)
    }
    
    convenience init(r: Int, g: Int, b: Int, a: CGFloat) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: a)
    }
    
    convenience init?(hexString: String) {
        guard let hex = hexString.hex else {
            return nil
        }
        self.init(hex: hex)
    }
    
    func hexStringFromColor() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"%06x", rgb)
    }
}
