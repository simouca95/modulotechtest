//
//  UserDefaults+Wrapper.swift
//  ModuloTech
//
//  Created by sami hazel on 09/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation


extension UserDefaults{
    
    func clearCache() {
        
        let defaults = UserDefaults.standard

        let domain = Bundle.main.bundleIdentifier!
        defaults.removePersistentDomain(forName: domain)
        defaults.synchronize()
        
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        defaults.synchronize()

    }
    
    // MARK: User proprities
    
    @UserDefault(key: "currentUser", defaultValue: nil)
    static var currentUser: Data?

    @UserDefault(key: "devices", defaultValue: nil)
    static var devices: Data?

}

@propertyWrapper
struct UserDefault<Value> {
    let key: String
    let defaultValue: Value
    var container: UserDefaults = .standard

    var wrappedValue: Value {
        get {
            return container.object(forKey: key) as? Value ?? defaultValue
        }
        set {
            container.set(newValue, forKey: key)
        }
    }
}
