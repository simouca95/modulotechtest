//
//  VerticalSlider.swift
//  ModuloTech
//
//  Created by sami hazel on 10/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import UIKit

class VerticalSlider: UISlider {
    
    @IBInspectable var trackHeight: CGFloat = 6
    
    @IBInspectable var thumbRadius: CGFloat = 24
    
    // Custom thumb view which will be converted to UIImage
    // and set as thumb. You can customize it's colors, border, etc.
    private lazy var thumbView: UIView = {
        let thumb = UIView()
        thumb.layer.borderColor = UIColor(.appBlack).cgColor
        thumb.layer.borderWidth = 6
        thumb.backgroundColor = .white
        return thumb
    }()
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        let thumb = thumbImage(radius: thumbRadius)
//        setThumbImage(thumb, for: .normal)
//
//        self.maximumTrackTintColor = UIColor(.appLightGrey)
//        self.minimumTrackTintColor = UIColor(.appBlack)
//
//    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let thumb = thumbImage(radius: thumbRadius)
        setThumbImage(thumb, for: .normal)
        
        self.maximumTrackTintColor = UIColor(.appLightGrey)
        self.minimumTrackTintColor = UIColor(.appBrown)
    }
    private func thumbImage(radius: CGFloat) -> UIImage {
        // Set proper frame
        // y: radius / 2 will correctly offset the thumb
        
        thumbView.frame = CGRect(x: 0, y: radius / 2, width: radius, height: radius)
        thumbView.layer.cornerRadius = radius / 2
        
        // Convert thumbView to UIImage
        // See this: https://stackoverflow.com/a/41288197/7235585
        
        let renderer = UIGraphicsImageRenderer(bounds: thumbView.bounds)
        return renderer.image { rendererContext in
            thumbView.layer.render(in: rendererContext.cgContext)
        }
    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        // Set custom track height
        // As seen here: https://stackoverflow.com/a/49428606/7235585
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        return newRect
    }
    
}
