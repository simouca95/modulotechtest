//
//  DataManager.swift
//  ModuloTech
//
//  Created by sami hazel on 11/04/2021.
//  Copyright © 2021 sami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class DataManager {
    
    private init(){}
    
    static let shared = DataManager()
    
    var devicesData = BehaviorRelay<[Device]?>(value: nil)
    
    var userData = BehaviorRelay<User?>(value: nil)    
    
    func loadHomeData(completion : @escaping (() -> Void)){
        
        if self.devicesData.value == nil || self.userData.value == nil{

            HomeApiCall.instance.loadData(onSuccess: { [weak self] homeData in
                
                self?.userData.accept(homeData?.user)
                
                self?.devicesData.accept(homeData?.devices)

                completion()
                
            }, onFailure: { _ in
                
                completion()

            })
            
        }

    }
    
    func deleteDevice(device : Device){
        
        var devices = self.devicesData.value
        
        devices?.removeAll(where: { $0.id == device.id})
        
        self.devicesData.accept(devices)

    }
    
    
    func updateUser(user : User){
        
        self.userData.accept(user)
    }
    
    
    func refreshDevices(){
        
        self.devicesData.accept(self.devicesData.value)
        
    }
    
    
    
    
    
}
